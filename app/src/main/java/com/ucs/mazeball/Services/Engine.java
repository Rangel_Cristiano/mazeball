package com.ucs.mazeball.Services;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ucs.mazeball.Models.Map;
import com.ucs.mazeball.Views.GameView;
import com.ucs.mazeball.Models.Enums.Movement;

public class Engine {

    private TextView mLevelView, mMoveView;
    private boolean isFinished;
    private SensorManager mSensorManager;
    private Vibrator mVibrator;
    private static float ACCEL_THRESHOLD = 2;
    private float mAccelX = 0;
    private float mAccelY = 0;
    private Map mMap;
    private BallEngine mBallEngine;
    private int mCurrentMap = 0;
    private int mMapToLoad = 0;
    private Movement mCommandedRollMovement = Movement.STOP;
    private GameView mGameView;
    private Context mContext;
    private boolean mSensorEnabled = true;
    private Button mRestartButton;

    private final SensorListener mSensorAccelerometer = new SensorListener() {

        public void onSensorChanged(int sensor, float[] values) {
            if (!mSensorEnabled) return;

            mAccelX = values[0];
            mAccelY = values[1];

            mCommandedRollMovement = Movement.STOP;
            if (Math.abs(mAccelX) > Math.abs(mAccelY)) {
                if (mAccelX < -ACCEL_THRESHOLD) mCommandedRollMovement = Movement.LEFT;
                if (mAccelX > ACCEL_THRESHOLD) mCommandedRollMovement = Movement.RIGHT;
            } else {
                if (mAccelY < -ACCEL_THRESHOLD) mCommandedRollMovement = Movement.DOWN;
                if (mAccelY > ACCEL_THRESHOLD) mCommandedRollMovement = Movement.UP;
            }

            if (mCommandedRollMovement != Movement.STOP && !mBallEngine.isRunning()) {
                mBallEngine.roll(mCommandedRollMovement);
            }
        }

        public void onAccuracyChanged(int sensor, int accuracy) {
        }
    };

    public Engine(Context context) {
        mContext = context;

        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorAccelerometer, SensorManager.SENSOR_ACCELEROMETER,
                SensorManager.SENSOR_DELAY_GAME);

        mMap = new Map(Map.mapsList.get(mCurrentMap));

        mBallEngine = new BallEngine(
                mContext,
                this,
                mMap,
                mMap.getInitialPositionX(),
                mMap.getInitialPositionY());
    }

    public void finishMap() {
        isFinished = true;

        mMapToLoad = (mCurrentMap + 1) % Map.mapsList.size();

        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(
                        mContext,
                        RingtoneManager.TYPE_ALL);

                final MediaPlayer mp = MediaPlayer.create(mContext, actualDefaultRingtoneUri);
                mp.start();

                if (mCurrentMap + 1 == Map.mapsList.size()) {
                    new AlertDialog.Builder(mContext)
                            .setCancelable(true)
                            .setTitle("Parabéns! Jogo concluído!")
                            .setPositiveButton("Reiniciar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    loadMap();

                                    mp.stop();
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    ((Activity) mContext).finish();
                                    mp.stop();

                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();
                } else {
                    new AlertDialog.Builder(mContext)
                            .setCancelable(true)
                            .setTitle("Nível concluído!")
                            .setPositiveButton("Próximo mapa", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                mp.stop();

                                loadMap();

                                dialog.cancel();
                                }
                            })
                            .create()
                            .show();
                }
            }
        });

    }

    public void loadMap() {
        mMapToLoad = (mCurrentMap + 1) % Map.mapsList.size();

        mCurrentMap = mMapToLoad;

        int level = mCurrentMap + 1;
        mLevelView.setText("Labirinto \n Nível " + level);
        getMovesView().setText("Movimentos: " + 0);

        mBallEngine.stop();
        mMap = new Map(Map.mapsList.get(mCurrentMap));
        mBallEngine.setMap(mMap);
        mBallEngine.setX(mMap.getInitialPositionX());
        mBallEngine.setY(mMap.getInitialPositionY());
        mBallEngine.setXTarget(mMap.getInitialPositionX());
        mBallEngine.setYTarget(mMap.getInitialPositionY());
        mBallEngine.setMoves(0);

        mMap.init();

        mGameView.setLineSize();
        mGameView.invalidate();

        isFinished = false;

        Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(
                mContext,
                RingtoneManager.TYPE_NOTIFICATION);

        final MediaPlayer mp = MediaPlayer.create(mContext, actualDefaultRingtoneUri);
        mp.start();
    }

    public void initializeViews(GameView gameView, TextView levelView, TextView moveView, Button restartButton) {
        mGameView = gameView;
        mMoveView = moveView;
        mLevelView = levelView;
        mRestartButton = restartButton;
        mBallEngine.setMazeView(gameView);

        mRestartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMovesView().setText("Movimentos: " + 0);

                mBallEngine.stop();
                mMap = new Map(Map.mapsList.get(mCurrentMap));
                mBallEngine.setMap(mMap);
                mBallEngine.setX(mMap.getInitialPositionX());
                mBallEngine.setY(mMap.getInitialPositionY());
                mBallEngine.setXTarget(mMap.getInitialPositionX());
                mBallEngine.setYTarget(mMap.getInitialPositionY());
                mBallEngine.setMoves(0);

                mMap.init();

                mGameView.setLineSize();
                mGameView.invalidate();

                Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(
                        mContext,
                        RingtoneManager.TYPE_NOTIFICATION);

                final MediaPlayer mp = MediaPlayer.create(mContext, actualDefaultRingtoneUri);
                mp.start();
            }
        });
    }

    public BallEngine getBall() {
        return mBallEngine;
    }

    public Map getMap() {
        return mMap;
    }

    public TextView getLevelView() {
        return mLevelView;
    }

    public TextView getMovesView() {
        return mMoveView;
    }

    public boolean isFinished() {
        return isFinished;
    }
}
