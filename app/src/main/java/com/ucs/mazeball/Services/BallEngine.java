package com.ucs.mazeball.Services;

import android.content.Context;
import android.os.SystemClock;
import android.os.Vibrator;

import com.ucs.mazeball.Models.Enums.Movement;
import com.ucs.mazeball.Models.Map;
import com.ucs.mazeball.Services.Engine;
import com.ucs.mazeball.Models.Enums.Wall;
import com.ucs.mazeball.Views.GameView;

import java.util.Timer;
import java.util.TimerTask;

import static com.ucs.mazeball.Models.Enums.Movement.DOWN;
import static com.ucs.mazeball.Models.Enums.Movement.LEFT;
import static com.ucs.mazeball.Models.Enums.Movement.RIGHT;
import static com.ucs.mazeball.Models.Enums.Movement.UP;

public class BallEngine {

    private Vibrator mVibrator;
    private int mMovesCount = 0;
    private int mTargetPositionX;
    private int mTargetPositionY;
    private float mCurrentPositionX;
    private float mCurrentPositionY;
    private int mVelocityX = 0;
    private int mVelocityY = 0;
    private static float Velocity = 0.004f;
    private long Time;
    private long Time2;
    private static final int DT_TARGET = 1000 / 25;
    private Timer mTimerToSimulation;
    private boolean mIsRunningBall = false;
    private Movement mMovement = Movement.STOP;
    private Map mMap;
    private GameView mGameView;
    private Engine mEngine;

    public BallEngine(
            Context context,
            Engine engine,
            Map map,
            int init_x,
            int init_y) {

        mEngine = engine;
        mCurrentPositionX = init_x;
        mCurrentPositionY = init_y;
        mTargetPositionX = init_x;
        mTargetPositionY = init_y;

        mMap = map;

        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public boolean isRunning() {
        return mIsRunningBall;
    }

    public void setMazeView(GameView gameView) {
        mGameView = gameView;
    }

    public void setMap(Map map) {
        mMap = map;
    }

    private boolean isValidMove(int x, int y, Movement dir) {
        switch (dir) {
            case LEFT:
                if (x <= 0)
                    return false;

                if ((mMap.getWalls(x, y) & Wall.LEFT) > 0 ||
                        (mMap.getWalls(x - 1, y) & Wall.RIGHT) > 0)
                    return false;
                break;
            case RIGHT:
                if (x >= mMap.getSizeX() - 1)
                    return false;

                if ((mMap.getWalls(x, y) & Wall.RIGHT) > 0 ||
                        (mMap.getWalls(x + 1, y) & Wall.LEFT) > 0)
                    return false;
                break;
            case UP:
                if (y <= 0)
                    return false;

                if ((mMap.getWalls(x, y) & Wall.TOP) > 0 ||
                        (mMap.getWalls(x, y - 1) & Wall.BOTTOM) > 0) return false;
                break;
            case DOWN:
                if (y >= mMap.getSizeY() - 1)
                    return false;

                if ((mMap.getWalls(x, y) & Wall.BOTTOM) > 0 ||
                        (mMap.getWalls(x, y + 1) & Wall.TOP) > 0)
                    return false;
                break;
        }

        return true;
    }

    public synchronized boolean roll(Movement dir) {
        //nao deixa parar o movimento se nao chegou na parede
        if (mIsRunningBall || mEngine.isFinished())
            return false;

        if (dir == LEFT) {
            mVelocityX = -1;
            mVelocityY = 0;
        } else if (dir == RIGHT) {
            mVelocityX = 1;
            mVelocityY = 0;
        }
        else if (dir == UP) {
            mVelocityX = 0;
            mVelocityY = -1;
        }
        else if (dir == DOWN) {
            mVelocityX = 0;
            mVelocityY = 1;
        }

        mTargetPositionX = Math.round(mCurrentPositionX);
        mTargetPositionY = Math.round(mCurrentPositionY);

        while (isValidMove(mTargetPositionX, mTargetPositionY, dir)) {
            mTargetPositionX = mTargetPositionX + mVelocityX;
            mTargetPositionY = mTargetPositionY + mVelocityY;
        }

        if (mTargetPositionX == mCurrentPositionX && mTargetPositionY == mCurrentPositionY) {
            mVibrator.vibrate(100);

            return false;
        }

        mIsRunningBall = true;
        mMovement = dir;

        mMovesCount++;
        mEngine.getMovesView().setText("Movimentos: " + mMovesCount);

        Time = SystemClock.elapsedRealtime();
        TimerTask simTask = new TimerTask() {
            public void run() {
                doStep();
            }
        };
        mTimerToSimulation = new Timer(true);
        try {
            mTimerToSimulation.schedule(simTask, 0, DT_TARGET);
        } catch (Exception ex) {}

        doStep();
        return true;
    }

    public void stop() {
        if (!mIsRunningBall) return;

        mTimerToSimulation.cancel();
        mIsRunningBall = false;
        mCurrentPositionX = mTargetPositionX;
        mCurrentPositionY = mTargetPositionY;
        mGameView.postInvalidate();
    }

    private void doStep() {
        Time2 = SystemClock.elapsedRealtime();
        float dt = (float) (Time2 - Time);
        Time = Time2;

        // seta a proxima posição
        float x = mCurrentPositionX + mVelocityX * Velocity * dt;
        float y = mCurrentPositionY + mVelocityY * Velocity * dt;

        boolean reachedTarget = false;
        switch (mMovement) {
            case LEFT:
                if (x <= 1f * mTargetPositionX) {
                    x = mTargetPositionX;
                    reachedTarget = true;
                }
                break;
            case RIGHT:
                if (x >= 1f * mTargetPositionX) {
                    x = mTargetPositionX;
                    reachedTarget = true;
                }
                break;
            case UP:
                if (y <= 1f * mTargetPositionY) {
                    y = mTargetPositionY;
                    reachedTarget = true;
                }
                break;
            case DOWN:
                if (y > 1f * mTargetPositionY) {
                    y = mTargetPositionY;
                    reachedTarget = true;
                }
                break;
        }

        mCurrentPositionX = x;
        mCurrentPositionY = y;

        // verifica se chegou no objetivo
        if (mMap.getGoal(Math.round(mCurrentPositionX), Math.round(mCurrentPositionY)) == 1) {
            mMap.removeGoal(Math.round(mCurrentPositionX), Math.round(mCurrentPositionY));

            mEngine.finishMap();
        }

        if (reachedTarget) {
            mMovement = Movement.STOP;
            mVelocityX = 0;
            mVelocityY = 0;
            mIsRunningBall = false;
            mTimerToSimulation.cancel();
        }

        mGameView.postInvalidate();
    }

    public Movement getRollDirection() {
        return mMovement;
    }

    public float getX() {
        return mCurrentPositionX;
    }

    public float getY() {
        return mCurrentPositionY;
    }

    public void setX(float x) {
        mCurrentPositionX = x;
    }

    public void setY(float y) {
        mCurrentPositionY = y;
    }

    public void setXTarget(int x) {
        mTargetPositionX = x;
    }

    public void setYTarget(int y) {
        mTargetPositionY = y;
    }

    public void setMoves(int moves) {
        mMovesCount = moves;
    }
}
