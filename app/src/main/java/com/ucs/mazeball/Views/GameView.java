package com.ucs.mazeball.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RadialGradient;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;

import com.ucs.mazeball.R;
import com.ucs.mazeball.Services.BallEngine;
import com.ucs.mazeball.Services.Engine;
import com.ucs.mazeball.Models.Enums.Wall;

public class GameView extends View {
    private int mWidth;
    private float mXMin;
    private float mYMin;
    private float mXMax;
    private float mYMax;
    private float mLineSize;
    private Engine mEngine;
    private BallEngine mBallEngine;
    private float mBallX;
    private float mBallY;
    private int mMapWidth;
    private int mMapHeight;
    private int[][] mWalls;
    private int[][] mGoals;
    private RadialGradient goalGradient = new RadialGradient(0, 0, 1, getResources().getColor(R.color.finish_highlight), getResources().getColor(R.color.finish_shadow), TileMode.MIRROR);
    private int mDrawStep = 0;
    private Paint paint;
    private Matrix matrix = new Matrix();
    private Matrix scaleMatrix = new Matrix();

    private static final float WALL_WIDTH = 25;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setAntiAlias(true);

        mWidth = Math.min(getWidth(), getHeight());
        mXMin = WALL_WIDTH / 2;
        mYMin = WALL_WIDTH / 2;
        mXMax = Math.min(getWidth(), getHeight()) - WALL_WIDTH / 2;
        mYMax = mXMax;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = Math.min(w, h);
        mXMax = Math.min(w, h) - WALL_WIDTH / 2;
        mYMax = mXMax;

        setLineSize();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = Math.min(getMeasuredWidth(), getMeasuredHeight());
        setMeasuredDimension(mWidth, mWidth);
    }

    @Override
    public void onDraw(Canvas canvas) {
        mDrawStep = mDrawStep + 1;
        mBallEngine = mEngine.getBall();
        mMapWidth = mEngine.getMap().getSizeX();
        mMapHeight = mEngine.getMap().getSizeY();

        drawWalls(canvas);
        drawGoals(canvas);
        drawBall(canvas);
    }

    public void setEngine(Engine engine) {
        mEngine = engine;
    }

    public void setLineSize() {
        float xUnit = ((mXMax - mXMin) / mEngine.getMap().getSizeX());
        float yUnit = ((mYMax - mYMin) / mEngine.getMap().getSizeY());

        mLineSize = Math.min(xUnit, yUnit);
    }

    private void drawWalls(Canvas canvas) {
        paint.setColor(getResources().getColor(R.color.wall));
        paint.setStrokeWidth(WALL_WIDTH);
        paint.setStrokeCap(Cap.ROUND);

        mWalls = mEngine.getMap().getWalls();

        for (int y = 0; y < mMapHeight; y++) {
            for (int x = 0; x < mMapWidth; x++) {
                if ((mWalls[y][x] & Wall.TOP) > 0) {
                    canvas.drawLine(
                            mXMin + x * mLineSize,
                            mYMin + y * mLineSize,
                            mXMin + (x + 1) * mLineSize,
                            mYMin + y * mLineSize,
                            paint);
                }
                if ((mWalls[y][x] & Wall.RIGHT) > 0) {
                    canvas.drawLine(
                            mXMin + (x + 1) * mLineSize,
                            mYMin + y * mLineSize,
                            mXMin + (x + 1) * mLineSize,
                            mYMin + (y + 1) * mLineSize,
                            paint);
                }
                if ((mWalls[y][x] & Wall.BOTTOM) > 0) {
                    canvas.drawLine(
                            mXMin + x * mLineSize,
                            mYMin + (y + 1) * mLineSize,
                            mXMin + (x + 1) * mLineSize,
                            mYMin + (y + 1) * mLineSize,
                            paint);
                }
                if ((mWalls[y][x] & Wall.LEFT) > 0) {
                    canvas.drawLine(
                            mXMin + x * mLineSize,
                            mYMin + y * mLineSize,
                            mXMin + x * mLineSize,
                            mYMin + (y + 1) * mLineSize,
                            paint);
                }
            }
        }

        paint.setShader(null);
    }

    private void drawGoals(Canvas canvas) {
        paint.setShader(goalGradient);
        paint.setStyle(Style.FILL);
        scaleMatrix.setScale(mLineSize, mLineSize);

        mGoals = mEngine.getMap().getGoals();

        for (int y = 0; y < mMapHeight; y++) {
            for (int x = 0; x < mMapWidth; x++) {
                if (mGoals[y][x] > 0) {
                    matrix.setTranslate(
                            mXMin + x * mLineSize,
                            mYMin + y * mLineSize);
                    matrix.setConcat(matrix, scaleMatrix);
                    goalGradient.setLocalMatrix(matrix);
                    canvas.drawRect(
                            mXMin + x * mLineSize + mLineSize / 4,
                            mYMin + y * mLineSize + mLineSize / 4,
                            mXMin + (x + 1) * mLineSize - mLineSize / 4,
                            mYMin + (y + 1) * mLineSize - mLineSize / 4,
                            paint);
                }
            }
        }

        paint.setShader(null);
    }

    private void drawBall(Canvas canvas) {
        mBallX = mBallEngine.getX();
        mBallY = mBallEngine.getY();

        paint.setShader(new RadialGradient(
                mXMin + (mBallX + 0.35f) * mLineSize,
                mYMin + (mBallY + 0.35f) * mLineSize,
                mLineSize * 0.35f,
                getResources().getColor(R.color.ball_highlight),
                getResources().getColor(R.color.ball_shadow),
                TileMode.MIRROR
        ));

        paint.setStyle(Style.FILL);
        canvas.drawCircle(
                mXMin + (mBallX + 0.5f) * mLineSize,
                mYMin + (mBallY + 0.5f) * mLineSize,
                mLineSize * 0.4f,
                paint
        );
        paint.setShader(null);
    }
}
