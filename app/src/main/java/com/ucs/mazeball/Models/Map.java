package com.ucs.mazeball.Models;

import java.util.LinkedList;
import java.util.List;

import static com.ucs.mazeball.Models.Enums.Wall.BOTTOM;
import static com.ucs.mazeball.Models.Enums.Wall.LEFT;
import static com.ucs.mazeball.Models.Enums.Wall.RIGHT;
import static com.ucs.mazeball.Models.Enums.Wall.TOP;

public class Map {
    private MapDraw mDesign;

    private int[][] mGoals;
    private int mGoalCount;

    public Map(MapDraw design) {
        mDesign = design;
        init();
    }

    public void init() {
        if (mGoals == null)
            mGoals = new int[mDesign.getSizeY()][mDesign.getSizeX()];

        int[][] goals = mDesign.getGoals();
        for (int y = 0; y < mDesign.getSizeY(); y++)
            for (int x = 0; x < mDesign.getSizeX(); x++)
                mGoals[y][x] = goals[y][x];

        mGoalCount = mDesign.getGoalCount();
    }

    public int[][] getWalls() {
        return mDesign.getWalls();
    }

    public int getWalls(int x, int y) {
        return mDesign.getWalls(x, y);
    }

    public int[][] getGoals() {
        return mGoals;
    }

    public int getGoal(int x, int y) {
        return mGoals[y][x];
    }

    public void removeGoal(int x, int y) {
        mGoalCount = mGoalCount - mGoals[y][x];
        mGoals[y][x] = 0;
    }

    public int getSizeX() {
        return mDesign.getSizeY();
    }

    public int getSizeY() {
        return mDesign.getSizeY();
    }

    public int getInitialPositionX() {
        return mDesign.getInitialPositionX();
    }

    public int getInitialPositionY() {
        return mDesign.getInitialPositionY();
    }

    public static final List<MapDraw> mapsList = new LinkedList<MapDraw>();

    static {

        mapsList.add(new MapDraw(
                "1",
                4, 4,
                new int[][]{
                        {LEFT | TOP | BOTTOM, TOP | BOTTOM, TOP | BOTTOM, TOP | RIGHT},
                        {LEFT, BOTTOM, BOTTOM , RIGHT | BOTTOM },
                        {LEFT | BOTTOM , BOTTOM, BOTTOM , RIGHT },
                        {LEFT | BOTTOM, BOTTOM, BOTTOM, BOTTOM | RIGHT}
                },
                new int[][]{
                        {0, 0, 0, 0},
                        {0, 0, 0, 0},
                        {0, 0, 0, 0},
                        {1, 0, 0, 0}
                },
                0, 0
        ));

        mapsList.add(new MapDraw(
                "2",
                7, 7,
                new int[][]{
                        {LEFT | TOP | BOTTOM, TOP , TOP | RIGHT, TOP, TOP, TOP, TOP | RIGHT | LEFT},
                        {LEFT | TOP  , TOP , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| LEFT},
                        {LEFT | RIGHT , BOTTOM , 0 , 0, TOP, 0,  RIGHT },
                        {LEFT | RIGHT , 0 , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| LEFT},
                        {LEFT , BOTTOM | LEFT, 0, 0, BOTTOM , 0, RIGHT | LEFT},
                        {LEFT, TOP | BOTTOM | TOP,  TOP,  TOP, BOTTOM | TOP,  0,  RIGHT  | LEFT},
                        {LEFT | BOTTOM |TOP, TOP | BOTTOM  | TOP, BOTTOM | TOP |RIGHT, BOTTOM , BOTTOM ,  BOTTOM, BOTTOM | RIGHT  | LEFT}
                },
                new int[][]{
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 1}
                },
                0, 0
        ));

        mapsList.add(new MapDraw(
                "Mapa 3",
                14, 14,
                new int[][]{
                        {LEFT | TOP | BOTTOM, TOP , TOP | RIGHT, TOP, TOP, TOP, TOP  | LEFT, TOP | BOTTOM, TOP , TOP | RIGHT, TOP, TOP, TOP, TOP | RIGHT | LEFT},
                        {LEFT | TOP  , TOP , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| LEFT, LEFT | TOP  , TOP , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| LEFT},
                        {LEFT | RIGHT , BOTTOM , 0 , 0, TOP, 0,  RIGHT , LEFT | RIGHT , BOTTOM , 0 , BOTTOM, TOP, 0,  RIGHT | LEFT},
                        {LEFT | RIGHT , 0 , LEFT | RIGHT, RIGHT, TOP, 0,  0, 0 , 0 , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| LEFT},
                        {LEFT , BOTTOM | LEFT, 0, 0, BOTTOM , 0, RIGHT | LEFT, 0 , BOTTOM | LEFT, 0, 0, BOTTOM , 0, RIGHT | LEFT},
                        {LEFT, TOP | BOTTOM | TOP,  TOP,  TOP, BOTTOM | TOP,  0,  RIGHT  | LEFT, 0 , BOTTOM | LEFT, 0, 0, BOTTOM , 0, RIGHT },
                        {LEFT, TOP | BOTTOM | TOP,  TOP,  TOP, BOTTOM | TOP,  0,  RIGHT  | LEFT, 0 , BOTTOM | LEFT, 0, 0, BOTTOM , 0, RIGHT | LEFT},
                        {LEFT | RIGHT , 0 , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| LEFT, 0 , 0 , LEFT | RIGHT, 0, TOP, 0,  RIGHT| LEFT},
                        {LEFT | RIGHT , 0 , LEFT , LEFT, TOP, 0,  RIGHT , BOTTOM , 0 , 0 , 0, TOP, 0,  RIGHT},
                        {LEFT | RIGHT , 0 , LEFT | RIGHT, LEFT, TOP, 0,  0, 0  , 0 ,  RIGHT, 0, TOP, 0,  RIGHT| LEFT},
                        {LEFT | RIGHT , 0 , LEFT , 0, TOP, 0,  RIGHT| LEFT, LEFT | RIGHT , 0 , LEFT | RIGHT, 0, TOP, 0,  RIGHT| LEFT},
                        {LEFT  , TOP , LEFT | RIGHT, RIGHT, 0, 0,  RIGHT| LEFT, LEFT   , TOP , LEFT | RIGHT, 0, TOP, 0,  RIGHT},
                        {LEFT , TOP , LEFT | RIGHT, RIGHT, TOP, 0,  RIGHT| BOTTOM, LEFT  , TOP , LEFT | RIGHT, 0, TOP, 0,  RIGHT| LEFT},
                        {LEFT | BOTTOM, TOP | BOTTOM  | TOP, BOTTOM | TOP |RIGHT, BOTTOM , BOTTOM , BOTTOM, BOTTOM | RIGHT  | LEFT, LEFT | BOTTOM, BOTTOM | LEFT, BOTTOM, BOTTOM, BOTTOM , BOTTOM, RIGHT | LEFT | BOTTOM}

                },
                new int[][]{
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                },
                0, 0
        ));
    }
}
