package com.ucs.mazeball.Models.Enums;

public enum Movement {
    STOP,
    LEFT,
    RIGHT,
    UP,
    DOWN
}
