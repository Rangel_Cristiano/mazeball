package com.ucs.mazeball.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.ucs.mazeball.Services.Engine;
import com.ucs.mazeball.Views.GameView;
import com.ucs.mazeball.R;

public class MainActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        ActionBar mActionBar = ((AppCompatActivity) this).getSupportActionBar();
        mActionBar.setTitle("Labirinto");

        new AlertDialog.Builder(this)
                .setTitle("Regras")
                .setMessage("O objetivo do jogo é chegar ao ponto vermelho com o menor número possível de movimentos.\n\n" +
                        "Onde, os mesmos, só podem ser efetuados ao chegarem na parede e não podem ser cancelados uma vez que ordenados. ")
                .setPositiveButton("Iniciar Jogo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setContentView(R.layout.main_activity);

                        Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(
                                mContext,
                                RingtoneManager.TYPE_NOTIFICATION);

                        final MediaPlayer mp = MediaPlayer.create(mContext, actualDefaultRingtoneUri);
                        mp.start();

                        initGame();
                    }
                })
                .show();
    }

    private void initGame() {
        Engine engine = new Engine(this);

        GameView gameView = findViewById(R.id.maze_view);
        TextView levelView = findViewById(R.id.maze_level_textview);
        TextView movesView = findViewById(R.id.moves_textview);
        Button restartButton = findViewById(R.id.restart_button);

        engine.initializeViews(gameView, levelView, movesView, restartButton);
        gameView.setEngine(engine);
        gameView.setLineSize();

    }
}